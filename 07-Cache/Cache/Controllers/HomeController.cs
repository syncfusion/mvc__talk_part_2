﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cache.Models;

namespace Cache.Controllers
{
    public class HomeController : Controller
    {
        private static List<Customer> customers;

        static HomeController()
        {
            customers = new List<Customer>();
            customers.Add(new Customer(){Id = 1, FirstName = "Jack", LastName = "Johnson", Frequency = ShoppingFrequency.Frequent});
            customers.Add(new Customer() { Id = 2, FirstName = "Jimmy", LastName = "Jackson", Frequency = ShoppingFrequency.Rare });
            customers.Add(new Customer() { Id = 3, FirstName = "Jimmy", LastName = "Jackson", Frequency = ShoppingFrequency.Rare });
            customers.Add(new Customer() { Id = 4, FirstName = "Amy", LastName = "Jackson", Frequency = ShoppingFrequency.MVP });
            customers.Add(new Customer() { Id = 5, FirstName = "Lucy", LastName = "Smith", Frequency = ShoppingFrequency.Frequent });
        }

        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View(customers);
        }

        //
        // GET: /Home/Details/5
        [OutputCache(Duration=3600, VaryByParam="none")]
        public ActionResult Cached()
        {
            return View(customers);
        }

        //
        // GET: /Home/Create
         [OutputCache(Duration = 3600, VaryByParam = "id")]
        public ActionResult CachedById(int id)
        {
            var customer = customers.Find(c => c.Id == id);
            
            return View(customer);
        }
    }
}
