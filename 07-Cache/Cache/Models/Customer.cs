﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cache.Models
{
    public enum ShoppingFrequency
    {
        Rare,
        Frequent,
        MVP
    }
    
    public class Customer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [DisplayName("Shopping frequency")]
        public ShoppingFrequency Frequency { get; set; }
    }
}