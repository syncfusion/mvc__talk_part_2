﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ErrorHandling.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        [HandleError(View = "CustomError", ExceptionType = typeof(ApplicationException))]
        public ActionResult Index()
        {
            throw new ApplicationException("Unexpected error occured");
            return View();
        }

        public ActionResult Index2()
        {
            throw new ApplicationException("Unexpected error occured");
            return View();
        }

    }
}
