﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomActionFilterTest.Controllers
{

    public class HomeController : Controller
    {
        //
        // GET: /Home/
        [TraceActionFilter]
        [CopyrightActionFilter]
        public ActionResult Index()
        {
            return Content("Hello");
        }

    }
}
