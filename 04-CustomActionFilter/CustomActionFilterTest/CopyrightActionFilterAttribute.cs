﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomActionFilterTest
{
    
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class CopyrightActionFilterAttribute : ActionFilterAttribute
    {
       
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (filterContext.Result is ContentResult)
            {
                var content = filterContext.Result as ContentResult;
                content.Content = string.Format("{0}<div>Copyright {1}. This note was inserted by the CoyrightActionFilter.</div>", content.Content,DateTime.Now.Year);
            }
        }

    }
}