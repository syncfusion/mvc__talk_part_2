﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Simple.Models;

namespace Simple.Controllers
{
    public class CustomersController : Controller
    {
        private static List<Customer> customers;
        
        static CustomersController()
        {
            customers = new List<Customer>();
            customers.Add(new Customer(){Id = 1, FirstName = "Jack", LastName = "Johnson", Frequency = ShoppingFrequency.Frequent});
            customers.Add(new Customer() { Id = 2, FirstName = "Jimmy", LastName = "Jackson", Frequency = ShoppingFrequency.Rare });
            customers.Add(new Customer() { Id = 2, FirstName = "Jimmy", LastName = "Jackson", Frequency = ShoppingFrequency.Rare });
            customers.Add(new Customer() { Id = 2, FirstName = "Amy", LastName = "Jackson", Frequency = ShoppingFrequency.MVP });
            customers.Add(new Customer() { Id = 2, FirstName = "Lucy", LastName = "Smith", Frequency = ShoppingFrequency.Frequent });
        }

        //
        // GET: /Customers/

        public ActionResult Index()
        {
            return View(customers);
        }

        //
        // GET: /Customers/Details/5

        public ActionResult Details(int id)
        {
            
            return View();
        }

        //
        // GET: /Customers/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Customers/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Customers/Edit/5

        public ActionResult Edit(int id)
        {
            var customer = customers.Find(c => c.Id == id);
            return View(customer);
        }

        //
        // POST: /Customers/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var customer = customers.Find(c => c.Id == id);
                TryUpdateModel(customer);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Customers/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Customers/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
