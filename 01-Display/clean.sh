find -type d -name 'packages' -exec rm -rfv {} \;
find -type d -name 'bin' -exec rm -rfv {} \;
find -type d -name 'obj' -exec rm -rfv {} \;
find -type d -name '_ReSharper.*' -exec rm -rfv {} \;
find -type d -name 'Backup' -exec rm -rfv {} \;
find -type d -name '_UpgradeReport_Files' -exec rm -rfv {} \;